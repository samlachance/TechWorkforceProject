# Hello World!!!

My name's Sam and I'm a full-stack Ruby on Rails engineer. I created this git repo so I could follow along with Tech Town's Tech Workforce Program. If you have any questions about the coursework, you can email me at <sam@samlachance.com> or message me on the TWP slack!

## Some things about me...

- I have a cat named Roscoe.
- I am a ham radio operator (callsign N3SAM).
- I went to elementary school with Miley Cyrus.

## Social media

- [Twitter: @samlachance](https://twitter.com/samlachance)
- [GitLab: @samlachance](https://gitlab.com/samlachance)
- [LinkedIn: in/samlachance](https://www.linkedin.com/in/samlachance/)

*To understand recursion, one must first understand recursion. -Stephen Hawking*